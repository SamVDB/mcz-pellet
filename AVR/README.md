This program can be flashed on an ethernut 2 board ( http://ethernut.de/en/hardware/enut2/index.html ) without any changes.

If you then connect pin PD2 via an NPN transistor (BC547C) to the TX pin of the RF module of the MCZ pellet remote control ( this one http://img8.bricozone.be/4620650cb942aa16f2.jpg ), then you'll be able to control your MCZ pellet stove via the ATMega128 microcontroller on the board.
Note: don't forget to power the RF module on the PCB!
For a connection layout: see https://bitbucket.org/SamVDB/mcz-pellet/raw/ce07bf08b19af6c877ecb4e92db3c74a59e0893a/LAYOUT.png

PORTING GUIDE:
//TODO: write something about the period of the timer that is used
