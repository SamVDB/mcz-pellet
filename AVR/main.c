#define F_CPU 14745600UL

#include <stdint.h>
#include <stdio.h>
#include <avr/interrupt.h>
#include <avr/io.h>
#include <util/delay.h>

#define DATA_PIN PD2
#define DATA_PIN_DDR DDRD
#define DATA_PIN_PORT PORTD

#define BITS_PER_WORD 12
#define DATA_SIZE 7         //Amount of words to send!
#define PACKETS_PER_BURST 5 //Amount of packets that are sent per burst
#define WORD_IDLE_BITS 4    //Amount of idle bits between two words
#define PACKET_IDLE_BITS 11 //Amount of idle bits between two packets



uint32_t on[] = {0xA8B, 0xC69, 0xC81, 0xD23, 0x83B, 0x9FF, 0x829};
uint32_t off[] = {0xA8B, 0xC69, 0xC81, 0x823, 0x83B, 0x867, 0xD31};

uint32_t *command;

volatile uint8_t firstPart = 1;
volatile uint8_t bit = 0;
volatile uint8_t idx = 0;
volatile uint8_t word_idle_time = 0;
volatile uint8_t packet_idle_time = 0;
volatile uint8_t transmit_word_finished = 0; 
volatile uint8_t transmit_packet_finished = 0;
volatile uint8_t packet_count = 0;

static inline void set_pin() {
    //DATA_PIN_PORT |= (1 << DATA_PIN); 
    DATA_PIN_PORT &= ~(1 << DATA_PIN);  //Inverted mode
}

static inline void clear_pin() {
    //DATA_PIN_PORT &= ~(1 << DATA_PIN);
    DATA_PIN_PORT |= (1 << DATA_PIN);   //Inverted mode
}

static inline void send_init_signals() {
    set_pin();
    _delay_ms(50);
    clear_pin();
    _delay_ms(50);
    
    clear_pin();
    _delay_us(420);
    set_pin();
    _delay_us(400);
}

int main(void) 
{
    command = off;

    /* Setup PORTD */
    DATA_PIN_DDR |= (1 << DATA_PIN);
    
    send_init_signals();

    /* Setup Timer/Counter1 */
    OCR1A = 0x1840;
    TCCR1B |= (1 << CS10);
    TCCR1B |= (1 << WGM12);
    TIMSK |= (1 << OCIE1A);

    sei();  //Enable interrupts
    
    while(1) 
    {

    }

    return 0;
}


ISR(TIMER1_COMPA_vect) {
    if(packet_count >= PACKETS_PER_BURST)
    {
        clear_pin();
        TCCR1B &= ~(1 << CS10); //Disable the timer
        return;
    }
    else if(transmit_packet_finished) 
    {
        /* Generate idle time between 2 packets */
        clear_pin();
        packet_idle_time++;
        if(packet_idle_time >= (PACKET_IDLE_BITS -1)) {
            transmit_packet_finished = 0;
            packet_idle_time = 0;
        }        
        return;
    }
    else if(transmit_word_finished) 
    {
        /* Generate idle time between 2 words */
        if(word_idle_time > 0 && idx < (DATA_SIZE-1)) 
            set_pin();

        word_idle_time++;

        if(word_idle_time >= (WORD_IDLE_BITS -1)) 
        {
            transmit_word_finished = 0; //Ready to send new word
            bit = 0;
            word_idle_time = 0;
            idx++;  //Set idx to the next byte
            if( idx >= DATA_SIZE ) {
                idx=0;  //All words are already sent. Reset idx.
                packet_count++; //All words are sent so increase packet_count
                transmit_packet_finished = 1;
            }
        }
        return;
    }

    /* Manchester encoder */

    if( ((command[idx] >> bit) & 0x01) == 1 ) {
        if(firstPart) {
            set_pin();
            firstPart = 0;
        }
        else {
            clear_pin();
            firstPart = 1;
            bit++;
        }
    }
    else {
        if(firstPart) {
            clear_pin();
            firstPart = 0;
        }
        else {
            set_pin();
            firstPart = 1;
            bit++;
        }
    }

    if(bit == BITS_PER_WORD) { 
        transmit_word_finished = 1;
    }
}
